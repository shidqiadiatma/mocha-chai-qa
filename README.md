# mochai-example
API automation test example project

- Inside the Project
    - MochaJS
    - Chai
    - Supertest
    - Mochawesome

- Install Dependencies
    - `npm install`

- Run The Test
    - Run all test
        - `npm run test-all`
    - Run specific test
        - `npm run test-auth`
        - `npm run test-user`
    - Run test by Tag
        - `npm run test -- --grep "@Positive"`